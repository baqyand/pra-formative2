class Test5 {
public static void main(String[] args)

//yang terjadi saat program di jalankan adalah 
// outputnya menjadi ke pinggir : 1 2 3
// itu karena outputnya memakai print saja sehingga menjadi ke pinggir
    {
        int arr[] = { 1, 2, 3 };

        // final with for-each statement
//perulangan foreach di mana int i mengulang dari tipe data array
//menggunakan foreach single line
        for (final int i : arr)
            System.out.print(i + " ");
    }
}
