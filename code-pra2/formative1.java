public class Test {
//berikut adalah code untuk main class
    public static void main(String[] args) {
//inisialisasi object dari constructor
        Test obj = new Test();

//object lalu menjalankan function start
        obj.start();
    }
    public void start() {
//saat di function start kita bisa melihat ada 2 string yang mana masing masing memiliki nilai
        String stra = "do";
//namun di string strb itu di tambahkan method lagi dan memiliki constructor tipe data string stra
        String strb = method(stra);
//maka strb bernilai do good
//outputnya adalah : do good
//sehingga jika di compile maka ouputnya adalah dogood: do good
        System.out.print(": "+stra + strb);
    }
//berikut adalah function method yang berisi data string dan inisialisasi data stringnya
    public String method(String stra) {
        stra = stra + "good";
//data stra di tambah dengan kata "good"
//sehingga output yang di keluarkan adalah dogood
        System.out.print(stra);

//kemudian function perlu mengembalikan nilai yaitu good

//sehingga output keseluruhan adalah dogood: do good
        return " good";
    }
}
